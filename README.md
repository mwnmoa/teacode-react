# teacode-react-contacts

## Getting Started
To run project locally:

1. Clone repository 

`git clone git@gitlab.com:mwnmoa/teacode-react.git` with SSH

`git clone https://gitlab.com/mwnmoa/teacode-react.git` with HTTPS

2. Install dependencies

`npm install` with npm

`yarn` with yarn

3. Start project

`npm start` with npm

`yarn start` with yarn

