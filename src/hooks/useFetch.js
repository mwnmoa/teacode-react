import { useEffect, useState } from "react";

const useFetch = (url) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(url);

        const data = await res.json();
        setData(data);
      } catch (error) {
        setIsError(true);
        setIsLoading(false);
        throw Error("Could not fetch the data for that resource");
      }
      setIsLoading(false);
    };

    fetchData();
  }, [url]);

  return {
    data,
    isLoading,
    isError,
  };
};

export default useFetch;
