import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import ContactsList from "./components/ContactsList";
import Search from "./components/Search";
import useFetch from "./hooks/useFetch";

const App = () => {
  const {
    isLoading,
    isError,
    data: contacts,
  } = useFetch(
    "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
  );
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value.toLowerCase());
  };

  useEffect(() => {
    if (!isLoading) {
      const sorted = contacts.sort((a, b) =>
        a.last_name < b.last_name ? -1 : 1
      );
      const result = sorted.filter(
        (contact) =>
          contact.first_name.toLowerCase().startsWith(searchTerm) ||
          contact.last_name.toLowerCase().startsWith(searchTerm)
      );
      setSearchResult(result);
    }
  }, [isLoading, searchTerm, contacts]);

  if (isLoading) return <h1 className="text-center mt-5 h5">Loading...</h1>;
  if (isError)
    return <h1 className="text-center mt-5 h5">Something went wrong...</h1>;

  return (
    <>
      <Container fluid>
        <Row className="bg-dark text-white">
          <h1 className="h5 text-center p-2">Contacts</h1>
        </Row>
      </Container>
      <Container>
        <Row>
          <Col md={{ span: 10, offset: 1 }} xs={12} lg={{ span: 8, offset: 2 }}>
            <Search onChange={handleSearch} />
            <ContactsList contacts={searchResult} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default App;
