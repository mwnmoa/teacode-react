import { Form } from "react-bootstrap";
import Avatar from "./Avatar";
import Checkbox from "./Checkbox";
import Name from "./Name";
import "../styles/Contact.css";

const Contact = ({ contact, onChange, checked }) => {
  const { avatar, first_name, last_name, id } = contact;

  return (
    <li className="contact d-flex align-items-center justify-content-between mb-2">
      <Form.Label
        htmlFor={id}
        className="label flex-grow-1 mb-0 d-flex align-items-center "
      >
        <Avatar avatar={avatar} firstName={first_name} lastName={last_name} />
        <Name firstName={first_name} lastName={last_name} />
      </Form.Label>
      <Checkbox id={id} onChange={onChange} checked={checked} />
    </li>
  );
};

export default Contact;
