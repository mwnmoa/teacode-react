import { Form } from "react-bootstrap";

const Search = ({ onChange }) => {
  return (
    <Form className="mt-4 mb-4">
      <Form.Control
        type="text"
        placeholder="Search"
        onChange={onChange}
      ></Form.Control>
    </Form>
  );
};

export default Search;
