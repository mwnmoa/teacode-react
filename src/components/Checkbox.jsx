import { Form } from "react-bootstrap";
import { useState } from "react";

const Checkbox = ({ id, onChange, checked }) => {
  console.log("checked:", checked);

  return (
    <Form.Check
      type="checkbox"
      id={id}
      onChange={onChange}
      checked={checked}
      // value={checked}
    />
  );
};

export default Checkbox;
