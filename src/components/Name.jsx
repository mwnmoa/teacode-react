const Name = ({ firstName, lastName }) => {
  return <div className="name ps-3">{`${firstName} ${lastName}`}</div>;
};

export default Name;
