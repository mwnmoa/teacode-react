import Contact from "./Contact";
import { Form } from "react-bootstrap";
import { useState, useEffect, useRef } from "react";
import {
  AutoSizer,
  List,
  CellMeasurer,
  CellMeasurerCache,
} from "react-virtualized";

const ContactsList = ({ contacts }) => {
  const cache = useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 60,
    })
  );
  const [checkedContacts, setCheckedContacts] = useState([]);
  const [checked, setChecked] = useState(false);

  const handleChange = (event) => {
    const { checked, id } = event.currentTarget;
    setCheckedContacts((prev) =>
      checked ? [...prev, id] : prev.filter((checkedId) => checkedId !== id)
    );
  };

  useEffect(
    () => console.log("Selected contacts IDs:", checkedContacts),
    [checkedContacts]
  );

  return (
    <Form onChange={handleChange} className="d-flex justify-content-center ">
      <div style={{ width: "60%", height: "100vh" }}>
        <AutoSizer>
          {({ width, height }) => {
            return (
              <ul
                className="list"
                style={{
                  paddingLeft: 0,
                }}
              >
                <List
                  width={width}
                  height={height}
                  rowHeight={cache.current.rowHeight}
                  deferredMeasurementCache={cache.current}
                  rowCount={contacts.length}
                  rowRenderer={({ key, index, style, parent }) => {
                    const contact = contacts[index];
                    return (
                      <CellMeasurer
                        key={key}
                        cache={cache.current}
                        parent={parent}
                        columnIndex={0}
                        rowIndex={index}
                      >
                        <div key={key} style={style}>
                          <Contact
                            key={contact.id}
                            contact={contact}
                            onChange={handleChange}
                            checked={checkedContacts.includes(
                              contact.id.toString()
                            )}
                            style={{ height: { height } }}
                          />
                        </div>
                      </CellMeasurer>
                    );
                  }}
                />
              </ul>
            );
          }}
        </AutoSizer>
      </div>
    </Form>
  );
};

export default ContactsList;
