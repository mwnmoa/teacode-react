import "../styles/Avatar.css";

const Avatar = ({ avatar, firstName, lastName }) => {
  return (
    <div className="background">
      {avatar ? (
        <img src={avatar} alt={`${firstName} ${lastName}`} />
      ) : (
        firstName[0] + lastName[0]
      )}
    </div>
  );
};

export default Avatar;
